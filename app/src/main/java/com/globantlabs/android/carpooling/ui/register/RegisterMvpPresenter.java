package com.globantlabs.android.carpooling.ui.register;


import com.globantlabs.android.carpooling.di.PerActivity;
import com.globantlabs.android.carpooling.ui.base.MvpPresenter;

@PerActivity
public interface RegisterMvpPresenter<V extends RegisterMvpView,
        I extends RegisterMvpInteractor> extends MvpPresenter<V, I> {

}