package com.globantlabs.android.carpooling.ui.login;

import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.ui.base.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Single;

public class LoginInteractor extends BaseInteractor
        implements LoginMvpInteractor {

    @Inject
    public LoginInteractor(PreferencesHelper preferencesHelper,
                           ApiHelper apiHelper) {
        super(preferencesHelper, apiHelper);
    }

    @Override
    public Single<LoginResponse> sendLoginRequestToServer(LoginRequest loginRequest) {
        return getApiHelper().postLoginRequest(loginRequest);
    }

    @Override
    public Single<ProfileResponse> getProfileApiCall(String accessToken) {
        return getApiHelper().getProfile(accessToken);
    }
}
