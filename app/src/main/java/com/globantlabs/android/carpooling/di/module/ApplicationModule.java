package com.globantlabs.android.carpooling.di.module;

import android.app.Application;
import android.content.Context;

import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.prefs.AppPreferencesHelper;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.di.ApplicationContext;
import com.globantlabs.android.carpooling.di.PreferenceInfo;
import com.globantlabs.android.carpooling.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(Retrofit retrofit){
        return retrofit.create(ApiHelper.class);
    }

}
