package com.globantlabs.android.carpooling.ui.login;


import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.ui.base.BasePresenter;
import com.globantlabs.android.carpooling.utils.AppConstants;
import com.globantlabs.android.carpooling.utils.AppLogger;
import com.globantlabs.android.carpooling.utils.rx.SchedulerProvider;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class LoginPresenter<V extends LoginMvpView, I extends LoginMvpInteractor>
        extends BasePresenter<V, I> implements LoginMvpPresenter<V, I> {

    private static final String TAG = "LoginPresenter";

    @Inject
    public LoginPresenter(I mvpInteractor,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onGoogleLoginClick() {
        getMvpView().openGoogleSignInActivity();
    }

    @Override
    public void userIsAlreadyLoggued() {
        getMvpView().openRegisterActivity();
    }

    @Override
    public void singInResultReceived(GoogleSignInResult result) {
        AppLogger.d("singInResultReceived: " + result.getStatus().toString());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            doLoginRequest(acct);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    private void doLoginRequest(GoogleSignInAccount acct) {
        String email = acct.getEmail();
        String authCode  = acct.getServerAuthCode();
        AppLogger.d("Sending | Email: " + email + " - authCode: " + authCode);

        getMvpView().showLoading();

        getCompositeDisposable().add(getInteractor()
                .sendLoginRequestToServer(new LoginRequest(email, authCode))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse response) throws Exception {

                        AppLogger.d("Received | accessToken: " + response.getAccessToken() +
                                " - refreshToken: " + response.getRefreshToken());
                        getInteractor().updateUserTokens(
                                response.getAccessToken(),
                                response.getRefreshToken()
                        );

                        if (!isViewAttached()) {
                            return;
                        }
                        doProfileRequest(response.getAccessToken());

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();

                        AppLogger.e("Error in login: " + throwable.getMessage());
                        // TODO: handle the login error here
                        /*if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }*/
                    }
                }));
    }

    private void doProfileRequest(String accessToken) {

        AppLogger.d("Sending | accessToken: " + accessToken);

        getCompositeDisposable().add(getInteractor()
                .getProfileApiCall("bearer " + accessToken)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<ProfileResponse>() {
                    @Override
                    public void accept(ProfileResponse response) throws Exception {

                        AppLogger.d("Received | name: " + response.getName() +
                                " email: " + response.getEmail());

                        getInteractor().updateUserInfo(
                                response,
                                AppConstants.LoggedInMode.LOGGED_IN_MODE_GOOGLE);

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        getMvpView().openRegisterActivity();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();

                        AppLogger.e("Error in profile request: " + throwable.getMessage());
                        // TODO: handle the getProfile error here
                        /*if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }*/
                    }
                }));
    }
}
