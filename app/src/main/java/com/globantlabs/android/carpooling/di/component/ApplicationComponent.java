package com.globantlabs.android.carpooling.di.component;

import android.app.Application;
import android.content.Context;

import com.globantlabs.android.carpooling.CarPoolingApp;
import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.di.ApplicationContext;
import com.globantlabs.android.carpooling.di.module.ApplicationModule;
import com.globantlabs.android.carpooling.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(CarPoolingApp app);

    @ApplicationContext
    Context context();

    Application application();

    PreferencesHelper preferencesHelper();

    ApiHelper apiHelper();

}