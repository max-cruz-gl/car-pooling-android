package com.globantlabs.android.carpooling.ui.login;

import com.globantlabs.android.carpooling.di.PerActivity;
import com.globantlabs.android.carpooling.ui.base.MvpPresenter;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView,
        I extends LoginMvpInteractor> extends MvpPresenter<V, I> {

    void onGoogleLoginClick();

    void userIsAlreadyLoggued();

    void singInResultReceived(GoogleSignInResult result);

}
