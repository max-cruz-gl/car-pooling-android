package com.globantlabs.android.carpooling.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.globantlabs.android.carpooling.di.ActivityContext;
import com.globantlabs.android.carpooling.di.PerActivity;
import com.globantlabs.android.carpooling.ui.login.LoginInteractor;
import com.globantlabs.android.carpooling.ui.login.LoginMvpInteractor;
import com.globantlabs.android.carpooling.ui.login.LoginMvpPresenter;
import com.globantlabs.android.carpooling.ui.login.LoginMvpView;
import com.globantlabs.android.carpooling.ui.login.LoginPresenter;
import com.globantlabs.android.carpooling.ui.register.RegisterInteractor;
import com.globantlabs.android.carpooling.ui.register.RegisterMvpInteractor;
import com.globantlabs.android.carpooling.ui.register.RegisterMvpPresenter;
import com.globantlabs.android.carpooling.ui.register.RegisterMvpView;
import com.globantlabs.android.carpooling.ui.register.RegisterPresenter;
import com.globantlabs.android.carpooling.utils.rx.AppSchedulerProvider;
import com.globantlabs.android.carpooling.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;


@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> provideLoginPresenter(
            LoginPresenter<LoginMvpView, LoginMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterMvpPresenter<RegisterMvpView, RegisterMvpInteractor> provideRegisterPresenter(
            RegisterPresenter<RegisterMvpView, RegisterMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @PerActivity
    LoginMvpInteractor provideLoginMvpInteractor(LoginInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    RegisterMvpInteractor provideRegisterMvpInteractor(RegisterInteractor interactor) {
        return interactor;
    }

}
