package com.globantlabs.android.carpooling.data.network.model;

public class LoginRequest {

    private String email;
    private String googleToken;

    public LoginRequest(String email, String googleToken) {
        this.email = email;
        this.googleToken = googleToken;
    }

    public String getEmail() {
        return email;
    }

    public String getGoogleToken() {
        return googleToken;
    }
}
