package com.globantlabs.android.carpooling.ui.base;


import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.utils.AppConstants;

public interface MvpInteractor {

    ApiHelper getApiHelper();

    PreferencesHelper getPreferencesHelper();

    void setUserAsLoggedOut();

    void setAccessToken(String accessToken);

    void updateUserTokens(String accessToken, String refreshToken);

    void updateUserInfo(ProfileResponse response,
                        AppConstants.LoggedInMode loggedInMode
                        );

}
