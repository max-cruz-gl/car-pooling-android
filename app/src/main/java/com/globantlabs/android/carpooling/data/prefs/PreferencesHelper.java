package com.globantlabs.android.carpooling.data.prefs;


import com.globantlabs.android.carpooling.utils.AppConstants;

import javax.inject.Singleton;

@Singleton
public interface PreferencesHelper {

    int getUserLoggedInMode();

    void setUserLoggedInMode(AppConstants.LoggedInMode mode);


    String getUserName();

    void setUserName(String userName);

    String getUserLastName();

    void setUserLastName(String userName);

    String getPictureUrl();

    void setPictureUrl(String pictureUrl);

    String getUserSite();

    void setUserSite(String site);

    String getUserEmail();

    void setUserEmail(String email);

    String getAddress();

    void setAddress(String address);

    boolean getIsDriver();

    void setIsDriver(boolean driver);

    String getStatus();

    void setStatus(String status);

    String getUserStatus();

    void setUserStatus(String userStatus);

    String getLastAction();

    void setLastAction(String lastAction);

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getRefreshToken();

    void setRefreshToken(String refreshToken);

}
