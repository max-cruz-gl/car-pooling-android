package com.globantlabs.android.carpooling.ui.login;

import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.ui.base.MvpInteractor;

import io.reactivex.Single;

public interface LoginMvpInteractor extends MvpInteractor {

    Single<LoginResponse> sendLoginRequestToServer(LoginRequest loginRequest);

    Single<ProfileResponse> getProfileApiCall(String accessToken);
}
