package com.globantlabs.android.carpooling.ui.login;


import com.globantlabs.android.carpooling.ui.base.MvpView;

public interface LoginMvpView extends MvpView {

    void openGoogleSignInActivity();
    void openRegisterActivity();
}
