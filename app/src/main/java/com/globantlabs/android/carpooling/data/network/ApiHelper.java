package com.globantlabs.android.carpooling.data.network;

import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiHelper {

    @POST("/user/login")
    Single<LoginResponse> postLoginRequest(@Body LoginRequest loginRequest);

    @GET("/user/profile")
    Single<ProfileResponse> getProfile(@Header("Authorization") String accessToken);
}
