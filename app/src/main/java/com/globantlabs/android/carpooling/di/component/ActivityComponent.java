package com.globantlabs.android.carpooling.di.component;


import com.globantlabs.android.carpooling.di.PerActivity;
import com.globantlabs.android.carpooling.di.module.ActivityModule;
import com.globantlabs.android.carpooling.ui.login.LoginActivity;
import com.globantlabs.android.carpooling.ui.register.RegisterActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

}
