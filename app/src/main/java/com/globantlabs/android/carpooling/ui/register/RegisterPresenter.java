package com.globantlabs.android.carpooling.ui.register;


import com.globantlabs.android.carpooling.ui.base.BasePresenter;
import com.globantlabs.android.carpooling.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class RegisterPresenter<V extends RegisterMvpView, I extends RegisterMvpInteractor>
        extends BasePresenter<V, I> implements RegisterMvpPresenter<V, I> {


    @Inject
    public RegisterPresenter(I mvpInteractor,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

}